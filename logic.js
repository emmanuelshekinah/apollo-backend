const rp = require("request-promise");

const getAllPeople = async (search, page) => {
  try {

    const url = page===undefined ? 
      search === undefined
        ? `https://swapi.dev/api/people/`
        : `https://swapi.dev/api/people/?search=${search}`
        :  search === undefined
        ? `https://swapi.dev/api/people/?page=${page}`
        : `https://swapi.dev/api/people/?page=${page}`;
    const options = {
      method: "GET",
      uri: url,
      headers: {
        "Content-Type": "application/json",
      },
      json: true,
    };

    const res = await rp(options);
    const data = await res;

    const myPromise = new Promise((resolve, reject) => {
      const peoples = [];
      data.results.map((item) => {
        peoples.push({
          name: item.name,
          height: item.height,
          mass: item.mass,
          gender: item.gender,
          homeworld: item.homeworld,
        });
      });

      const next = data["next"];
      const prev = data["previous"];
      const array = {
        next: next === null ? null : next.replace("https://swapi.dev/api/", ""),
        previous:
          prev === null ? null : prev.replace("https://swapi.dev/api/", ""),
        peoples: peoples,
      };
      resolve(array);
    });
    return Promise.resolve(await myPromise);
  } catch (error) {
    console.log(error);
    return Promise.resolve({ status: "false", msg: error });
  }
};

module.exports = { getAllPeople };
