const express = require("express");
const cors = require("cors");
const logic = require('./logic')
const port = 4000


const app = express();

app.use(cors());

app.get("/people/", async(req, res) => {
    const peoples   =  await logic.getAllPeople(req.query.search, req.query.page);
    res.status(200).send({data: await peoples});
});

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`)
  })



